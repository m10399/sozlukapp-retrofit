package com.example.sozlukapp_retrofit

import android.os.Bundle
import android.util.Log
import android.view.Menu
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sozlukapp_retrofit.databinding.ActivityMainBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(),SearchView.OnQueryTextListener {
    private lateinit var binding: ActivityMainBinding
    private lateinit var kelimelerListe:ArrayList<Kelimeler>
    private lateinit var adapter:KelimelerAdapter
    private lateinit var kdi:KelimelerDaoInterface
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        binding.toolbar.title = "Sözlük Uygulaması"
        setSupportActionBar(binding.toolbar)

        binding.rv.setHasFixedSize(true)
        binding.rv.layoutManager = LinearLayoutManager(this)

       kdi = ApiUtils.getKelimelerDaoInterface()

        tumKelimeler()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu,menu)
        val item = menu?.findItem(R.id.action_ara)
        val searchView = item?.actionView as SearchView
        searchView.setOnQueryTextListener(this)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        aramaYap(query!!)
        Log.e("Enter tuşu",query!!)
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        aramaYap(newText!!)
        Log.e("Harf girdikçe",newText!!)
        return true
    }
    fun tumKelimeler(){
        kdi.tumKelimeler().enqueue(object:Callback<KelimelerCevap>{
            override fun onResponse(call: Call<KelimelerCevap>, response: Response<KelimelerCevap>?) {
                if (response !=  null){
                    val liste = response.body()!!.kelimeler
                    adapter = KelimelerAdapter(this@MainActivity,liste)
                    binding.rv.adapter = adapter
                }
            }

            override fun onFailure(p0: Call<KelimelerCevap>, p1: Throwable) {

            }

        })
    }
    fun aramaYap(aramaKelime:String){
        kdi.kelimeAra(aramaKelime).enqueue(object:Callback<KelimelerCevap>{
            override fun onResponse(call: Call<KelimelerCevap>, response: Response<KelimelerCevap>?) {
                if (response !=  null){
                    val liste = response.body()!!.kelimeler
                    if (liste != null){
                        adapter = KelimelerAdapter(this@MainActivity,liste)
                        binding.rv.adapter = adapter
                    }

                }
            }

            override fun onFailure(p0: Call<KelimelerCevap>, p1: Throwable) {

            }

        })
    }
}