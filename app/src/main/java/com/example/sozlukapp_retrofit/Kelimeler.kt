package com.example.sozlukapp_retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Kelimeler(
   @SerializedName("kelime_id") @Expose val kelime_id:Int,
   @SerializedName("ingilizce") @Expose val ingilizce:String,
   @SerializedName("turkce") @Expose val turkce:String):Serializable {}